# javascript-demo

Just a JavaScript demo, view on [Heroku](https://quiet-mountain-25635.herokuapp.com/). 

Source on [GitLab](https://gitlab.com/TomiToivio/javascript-demo/).

By [Tomi Toivio](mailto:tomi@sange.fi)
