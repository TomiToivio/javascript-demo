class characterBase {
  constructor(name, description, hp, strength, dexterity, constitution, intelligence, education, charisma, inventory, location, dungeon) {
    this.name = name;
    this.description = description;
    this.hp = hp; // Hit Points of the character.
    this.strength = strength; // How strong the character is.
    this.dexterity = dexterity; // How agile or coordinated the character is.
    this.constitution = constitution; // How healthy and fit the character is.
    this.intelligence = intelligence; // How intelligent the character is.
    this.education = education; // This measures how much the character knows.
    this.charisma = charisma; // This is a measure of the character's social skills, beauty and persuasiveness.
    this.inventory = inventory; // Array of items in character's inventory.
    this.location = location; // Location of the character on map.
    this.dungeon = dungeon; // Dungeon the character is in.
  }
  // Move north!
  moveNorth() {
    this.location.x += 1; 
  }
  // Move south!
  moveSouth() {
    this.location.x -= 1; 
  }
  // Move east!
  moveEast() {
    this.location.y += 1; 
  }
  // Move west!
  moveWest() {    
    this.location.y -= 1; 
  }
  // Move northeast!
  moveNorthEast() {
    this.location.x += 1; 
    this.location.y += 1; 
  }
  // Move northwest!
  moveNorthWest() {
    this.location.x += 1; 
    this.location.y -= 1; 
  }
  // Move southeast!
  moveSouthEast() {
    this.location.x -= 1; 
    this.location.y += 1; 
  }
  // Move southwest!
  moveSouthWest() {
    this.location.x -= 1; 
    this.location.y -= 1; 
  }
}