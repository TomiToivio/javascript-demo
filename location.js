class itemLocation {
    constructor(worldX, worldY, dungeonID, dungeonX, dungeonY) {
        this.worldX = worldX;
        this.worldY = worldY;
        this.dungeonID = dungeonID;
        this.dungeonX = dungeonX;
        this.dungeonY = dungeonY;
    }
}