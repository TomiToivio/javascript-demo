class diceRoll {
    constructor(numberOfDices, numberOfSides) {
        this.numberOfDices = numberOfDices;
        this.numberOfSides = numberOfSides;
        this.rollDice();
    }

    rollDice() {
        let diceTotal = 0;
        for (let i = 0; i < this.numberOfDices; i++) {
            diceTotal = diceTotal + Math.floor(Math.random() * this.numberOfSides) + 1;
        }
        return diceTotal;
    }
}